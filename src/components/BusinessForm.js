import React from 'react';
import apiClient from '../services/api';

const BusinessForm = () => {

    const [companyname, setCompanyname] = React.useState('');
    const [companyemail, setCompanyemail] = React.useState('');
    const [gstnumber, setGstnumber] = React.useState('');
    const [type, setType] = React.useState('');
    const [location, setLocation] = React.useState('');
    const [businessaddress, setBusinessaddress] = React.useState('');
    const [businessstate, setBusinessstate] = React.useState('');
    const [businesscity, setBusinesscity] = React.useState('');

    const [registerError, setRegisterError] = React.useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        setRegisterError(false);
        
        apiClient.post('/api/businessregister', {
            companyname: companyname,
            companyemail: companyemail,
            gstnumber: gstnumber,
            type: type,
            location: location,
            businessaddress: businessaddress,
            businessstate: businessstate,
            businesscity: businesscity
        }).then(response => {
            console.log(response);
        }
        ).catch(error => {
            if (error.response && error.response.status === 422) {
                setRegisterError(true);
            } else {
                console.error(error);
            }
        })
    
    }



    return (
        <>
        <div className="container bg">
    <h2>Business Credentials :</h2>
    <br/>
    <form onSubmit={handleSubmit}>
    <div className="form-row">
        <div className="form-group col-md-6">
            <label>Company Name :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="companyname" id="companyname" placeholder="Company name" value={companyname} onChange={e => setCompanyname(e.target.value)} required/>
        </div>
        <div className="form-group col-md-6">
            <label>Company Email :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="companyemail" id="companyemail" placeholder="Company email" value={companyemail} onChange={e => setCompanyemail(e.target.value)} required/>
        </div>
    </div>
    <div className="form-row">
    <div className="form-group col-md-6">
            <label>Gst Number :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="gstnumber" id="gstnumber" placeholder="GST Number" value={gstnumber} onChange={e => setGstnumber(e.target.value)}/>
        </div>
        <div className="form-group col-md-6">
            <label>Type:</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="type" id="type" placeholder="Company Type" value={type} onChange={e => setType(e.target.value)} required/>
        </div>
    </div>
    <div className="form-group">
        <label>Address :</label>
        <textarea className={"form-control" + (registerError ? ' is-invalid' : '')} name="businessaddress" id="businessaddress" rows="3" placeholder="Enter Full Address" value={businessaddress} onChange={e => setBusinessaddress(e.target.value)} required></textarea>
    </div>
    <div className="form-row">
    <div className="form-group col-md-4">
            <label>Location :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="location" id="state" placeholder="Location" value={location} onChange={e => setLocation(e.target.value)} required/>
        </div>
    <div className="form-group col-md-4">
            <label>State :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="businessstate" id="state" placeholder="State" value={businessstate} onChange={e => setBusinessstate(e.target.value)} required/>
        </div>
        <div className="form-group col-md-4">
            <label>City :</label>
            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="businesscity" id="city" placeholder="City" value={businesscity} onChange={e => setBusinesscity(e.target.value)} required/>
        </div>
    </div>
    <br/> 
        <div align="center">
            <button type="submit" className="btn btn-success">Update</button>
            <button type="reset" className="btn btn-danger">Cancel</button>
        </div>
    <br/>
    </form>
</div>

        </>
    )
}

export default BusinessForm;