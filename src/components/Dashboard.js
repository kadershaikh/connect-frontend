import React from 'react';
import { Link } from 'react-router-dom';


const Dashboard = (props) => {
    if (props.loggedIn) {
        return (
            <>
                <h1>Dashboard</h1>

                <div className="card">
                    
                    <div className="card-body">
                    
                        <p>Welcome To Connect web-app ..</p>
                    <div className="container">
                        <h4>Click Here To Complete Your Contact Profile :
                            <Link to="/contact-form"><button type="button" className="btn btn-success">Contact Details</button></Link>
                        </h4>
                    </div> 
                    
                    
                    <div className="container">
                        <h4>Click Here To Complete Your Business Profile :
                            <Link to="/business-form"><button type="button" className="btn btn-success">Business Details</button></Link>
                        </h4>
                    </div>
                    
                    </div>
                </div>

            </>
        );
    }
    return (
        <div className="alert alert-warning">You are not logged in.</div>
    );

};

export default Dashboard;