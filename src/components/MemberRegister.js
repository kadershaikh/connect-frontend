// import React from 'react';
// import apiClient from '../services/api';
// import { Redirect } from 'react-router-dom';

// const MemberRegister = () => {
//     const [name, setName] = React.useState('');
//     const [email, setEmail] = React.useState('');
//     const [password, setPassword] = React.useState('');
//     const [confirm_password, setConfirm_password] = React.useState('');
//     const [phone_number, setPhone_number] = React.useState('');
//     const [residential_address, setResidential_address] = React.useState('');
//     const [company_name, setCompany_name] = React.useState('');
//     const [company_email, setCompany_email] = React.useState('');
//     const [company_type, setCompany_type] = React.useState('');
//     const [company_location, setCompany_location] = React.useState('');

//     const [registerError, setRegisterError] = React.useState(false);
//     const [toLogin, setToLogin] = React.useState(false);

//     const handleSubmit = (e) => {
//         e.preventDefault();
//         setRegisterError(false);
//         apiClient.get('/sanctum/csrf-cookie')
//         .then(response => {
//         apiClient.post('/api/register', {
//             name: name,
//             email: email,
//             password: password,
//             confirm_password: confirm_password,
//             phone_number: phone_number,
//             residential_address: residential_address,
//             company_name: company_name,
//             company_email: company_email,
//             company_type: company_type,
//             company_location: company_location
//         }).then(response => {
//             setToLogin(true);
//         }
//         ).catch(error => {
//             if (error.response && error.response.status === 422) {
//                 setRegisterError(true);
//             } else {
//                 console.error(error);
//             }
//         })
//     });
//     }
//     if(toLogin === true) {
//         return <Redirect to='/login' />
//     }

//     return (
//         <>

//             <div className="container mb-5">

//                 <h3>Register</h3>

//                 <form onSubmit={handleSubmit}>
//                     <h4>Personal Details</h4>
//                     <div className="form-row">

//                         <div className="form-group col-md-6">
//                             <label htmlFor="name">Name</label>
//                             <input
//                                 type="text"
//                                 name="name"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Name"
//                                 value={name}
//                                 onChange={e => setName(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="email">Email</label>
//                             <input
//                                 type="email"
//                                 name="email"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Email"
//                                 value={email}
//                                 onChange={e => setEmail(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="password">Password</label>
//                             <input
//                                 type="password"
//                                 name="password"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Password with minimum 8 characters"
//                                 value={password}
//                                 onChange={e => setPassword(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="password">Confirm Password</label>
//                             <input
//                                 type="password"
//                                 name="confirm_password"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Confirm Password"
//                                 value={password}
//                                 onChange={e => setConfirm_password(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="phone_number">Phone Number</label>
//                             <input
//                                 type="text"
//                                 name="phone_number"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Phone Number"
//                                 value={phone_number}
//                                 onChange={e => setPhone_number(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="residential_address">Residential Address</label>
//                             <textarea
//                                 type="text"
//                                 name="residential_address"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Residential Address"
//                                 value={residential_address}
//                                 onChange={e => setResidential_address(e.target.value)}
//                                 required
//                             ></textarea>
//                         </div>
//                     </div>

//                     <h4>Business Details</h4>
//                     <div className="form-row">

//                         <div className="form-group col-md-6">
//                             <label htmlFor="company_name">Company Name</label>
//                             <input
//                                 type="text"
//                                 name="company_name"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Company Name"
//                                 value={company_name}
//                                 onChange={e => setCompany_name(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="company_email">Company Email</label>
//                             <input
//                                 type="email"
//                                 name="company_email"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Company Email"
//                                 value={company_email}
//                                 onChange={e => setCompany_email(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="company_type">Company Type</label>
//                             <input
//                                 type="text"
//                                 name="company_type"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Company Type"
//                                 value={company_type}
//                                 onChange={e => setCompany_type(e.target.value)}
//                                 required
//                             />
//                         </div>
//                         <div className="form-group col-md-6">
//                             <label htmlFor="company_location">Company Address</label>
//                             <textarea
//                                 type="text"
//                                 name="company_location"
//                                 className={"form-control" + (registerError ? ' is-invalid' : '')}
//                                 placeholder="Company Address"
//                                 value={company_location}
//                                 onChange={e => setCompany_location(e.target.value)}
//                                 required
//                             ></textarea>
//                         </div>
//                     </div>

//                     {registerError ? <div className="alert alert-danger">There was an error submitting your details.</div> : null}
//                     <button type="submit" className="btn btn-primary">Register</button>
//                 </form>
//             </div>
//         </>
//     );

// }

// export default MemberRegister;