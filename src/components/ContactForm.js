import React from 'react';
import apiClient from '../services/api';

const ContactForm = () => {
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [residentialaddress, setResidentialaddress] = React.useState('');
    const [phonenumber, setPhonenumber] = React.useState('');
    const [country, setCountry] = React.useState('');
    const [state, setState] = React.useState('');
    const [city, setCity] = React.useState('');

    const [registerError, setRegisterError] = React.useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        setRegisterError(false);
        
        apiClient.post('/api/memberregister', {
            name: name,
            email: email,
            residentialaddress: residentialaddress,
            phonenumber: phonenumber,
            country: country,
            state: state,
            city: city,
        }).then(response => {
            console.log(response);
        }
        ).catch(error => {
            if (error.response && error.response.status === 422) {
                setRegisterError(true);
            } else {
                console.error(error);
            }
        })
    
    }

    return (
        <>
            <div className="container bg">
                <h2>Customer Credentials :</h2>
                <br />
                <form onSubmit={handleSubmit} >
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Name :</label>
                            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="name" id="name" placeholder="Name" value={name} onChange={e => setName(e.target.value)} required/>
                        </div>
                        <div className="form-group col-md-6">
                            <label>E-mail :</label>
                            <input type="email" className={"form-control" + (registerError ? ' is-invalid' : '')} name="email" id="email" placeholder="E-mail" value={email} onChange={e => setEmail(e.target.value)} required/>
                        </div>
                    </div>
                    
                    <div className="form-row">
                    <div className="form-group col-md-6">
                        <label>Residential Address :</label>
                        <textarea className={"form-control" + (registerError ? ' is-invalid' : '')} name="residentialaddress" id="address" rows="3" placeholder="Enter Full Address" value={residentialaddress} onChange={e => setResidentialaddress(e.target.value)} required></textarea>
                    </div>
                        <div className="form-group col-md-6">
                            <label>Phone Number :</label>
                            <input type="Number" className={"form-control" + (registerError ? ' is-invalid' : '')} name="phonenumber" id="phonenumber" placeholder="Phone number" value={phonenumber} onChange={e => setPhonenumber(e.target.value)} required/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-4">
                            <label>Country :</label>
                            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="country" id="country" placeholder="country" value={country} onChange={e => setCountry(e.target.value)} required/>
                        </div>

                        <div className="form-group col-md-4">
                            <label>State :</label>
                            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="state" id="state" placeholder="state" value={state} onChange={e => setState(e.target.value)} required/>
                        </div>
                        <div className="form-group col-md-4">
                            <label>City :</label>
                            <input type="text" className={"form-control" + (registerError ? ' is-invalid' : '')} name="city" id="city" placeholder="City" value={city} onChange={e => setCity(e.target.value)} required/>
                        </div>
                    </div>
                    <br />
                    <div align="center">
                    {registerError ? <div className="alert alert-danger">There was an error submitting your details.</div> : null}
                        <button type="submit" className="btn btn-success">Update</button>
                        <button type="reset" className="btn btn-danger">Cancel</button>
                    </div>
                    <br />
                </form>
            </div>
        </>
    )
}

export default ContactForm;