import React from 'react';
import apiClient from '../services/api';
import { Redirect } from 'react-router-dom';


const Register = (props) => {
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [mobile, setMobile] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [password_confirmation, setPassword_confirmation] = React.useState('');
    


    const [registerError, setRegisterError] = React.useState(false);
    const [toHome, setToHome] = React.useState(false);
                


    const handleSubmit = (e) => {
        e.preventDefault();
        setRegisterError(false);
        apiClient.get('/sanctum/csrf-cookie')
        .then(response => {
        apiClient.post('/api/register', {
            name: name,
            email: email,
            password: password,
            password_confirmation: password_confirmation,
            mobile: mobile,
        }).then(response => {
            props.login();
            setToHome(true);
        }
        ).catch(error => {
            if (error.response && error.response.status === 422) {
                setRegisterError(true);
            } else {
                console.error(error);
            }
        })
    });
    }

    if (toHome === true) {
        return <Redirect to='/' />
    }


    return (
        <>

            <div className="container mb-5 col-lg-6 offset-sm-3">

                <h3>Register</h3>

                <form onSubmit={handleSubmit}>
                
                    <div className="form-row ">

                        <div className="form-group col-md-6">
                            <label htmlFor="name">Name</label>
                            <input
                                type="text"
                                name="name"
                                className={"form-control" + (registerError ? ' is-invalid' : '')}
                                placeholder="Name"
                                value={name}
                                onChange={e => setName(e.target.value)}
                                required
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                name="email"
                                className={"form-control" + (registerError ? ' is-invalid' : '')}
                                placeholder="Email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="mobile">Mobile Number</label>
                            <input
                                type="text"
                                name="mobile"
                                className={"form-control" + (registerError ? ' is-invalid' : '')}
                                placeholder="Phone Number"
                                value={mobile}
                                onChange={e => setMobile(e.target.value)}
                                required
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                name="password"
                                className={"form-control" + (registerError ? ' is-invalid' : '')}
                                placeholder="Password with minimum 8 characters"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="password_confirmation">Confirm Password</label>
                            <input
                                type="password"
                                name="password_confirmation"
                                className={"form-control" + (registerError ? ' is-invalid' : '')}
                                placeholder="Confirm Password"
                                value={password_confirmation}
                                onChange={e => setPassword_confirmation(e.target.value)}
                                required
                            />
                        </div>              
                    </div>

                    {registerError ? <div className="alert alert-danger">There was an error submitting your details.</div> : null}
                    <button type="submit" className="btn btn-primary">Register</button>
                </form>
            </div>
        </>
    );

}


export default Register;