import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';
import BusinessPage from './components/BusinessPage';
import ContactsPage from './components/ContactsPage';
import Dashboard from './components/Dashboard';
import Login from './components/Login';
import Register from './components/Register';
import apiClient from './services/api';
import ContactForm from './components/ContactForm.js';
import BusinessForm from './components/BusinessForm.js';

const App = () => {
  const [loggedIn, setLoggedIn] = React.useState(
    sessionStorage.getItem('loggedIn') === 'true' || false
  );
  const login = () => {
    setLoggedIn(true);
    sessionStorage.setItem('loggedIn', true);
  };
  const logout = () => {
    apiClient.post('/api/logout').then(response => {
      setLoggedIn(false);
      sessionStorage.setItem('loggedIn', false);
    })
  };
  const authLink = loggedIn
    ? <>
      <li className="nav-item">
        <NavLink to='/contacts' className="nav-link">Contacts</NavLink>
      </li>
      <li className="nav-item">
        <NavLink to='/business' className="nav-link">Business</NavLink>
      </li>
      <li><button onClick={logout} className="nav-link btn btn-link">Logout</button></li>
    </>
    : <>
      <li className="nav-item">
        <NavLink to='/login' className="nav-link">Login</NavLink>
      </li>
      <li className="nav-item">
        <NavLink to='/register' className="nav-link">Register</NavLink>
      </li>
    </>
  return (
    <Router>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark fixed-top">
        <a className="navbar-brand">Connect</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink to='/' className="nav-link">Dashboard</NavLink>
            </li>

            {authLink}

          </ul>
        </div>
      </nav>
      <div className="container mt-5 pt-5">
        <Switch>
          {loggedIn && <Route path='/contacts' component={ContactsPage} />}
          {loggedIn && <Route path='/business' component={BusinessPage} />}
          <Route path='/' exact render={props => (
            <Dashboard {...props} loggedIn={loggedIn} />
          )} />
          <Route path='/login' render={props => (
            <Login {...props} login={login} />
          )} />
          <Route path='/register' render={props => (
            <Register {...props} login={login} />
          )} />
          {loggedIn && <Route path="/contact-form" component={ContactForm} />}
          {loggedIn && <Route path="/business-form" component={BusinessForm} />}
        </Switch>
      </div>
    </Router>
  );
};

export default App;